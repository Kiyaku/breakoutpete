﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomCamera : MonoBehaviour {
	public RenderTexture RenTex;
	public RawImage RenImage;
	float scale = 1f;
	void Update () {
		scale = 0.01f;

		int sizeX = Mathf.RoundToInt(1280f * scale);
		if (sizeX < 0)
			sizeX = 1;

		int sizeY = Mathf.RoundToInt(720f * scale);
		if (sizeY < 0)
			sizeY = 1;

		RenTex = new RenderTexture(sizeX, sizeY, 24, RenderTextureFormat.ARGB32);
		RenTex.filterMode = FilterMode.Point;
		GetComponent<Camera>().targetTexture = RenTex;
		RenImage.texture = RenTex;
	}
}
