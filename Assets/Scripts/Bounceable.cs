﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bounceable : MonoBehaviour {
	public SpriteRenderer BounceTarget;
	public float BounceScale = 1.5f;
	public float BounceSpeed = 8f;
	public int BounceSortingOrder = 5;
	private int tempOrder;


	private void Start() {
		tempOrder = BounceTarget.sortingOrder;
	}


	public void DoBounce() {
		if (BounceTarget != null) {
			StopAllCoroutines();
			StartCoroutine(Bounce());
		}
	}


	protected IEnumerator Bounce() {
		float timer = 0;
		Vector3 startScale = Vector3.one * BounceScale;
		transform.localScale = startScale;

		BounceTarget.sortingOrder = BounceSortingOrder;

		while (timer < 1) {
			timer += Time.deltaTime * BounceSpeed;
			transform.localScale = Vector3.Lerp(startScale, Vector3.one, Mathf.SmoothStep(0, 1, timer));
			yield return null;
		}

		BounceTarget.sortingOrder = tempOrder;

		transform.localScale = Vector3.one;
	}
}
