﻿[System.Serializable]
public class MapData {
	public int SizeX, SizeY;
	public int MapOffsetY;
	public int[,] MapArray;
}
