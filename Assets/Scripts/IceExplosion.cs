﻿using UnityEngine;

[RequireComponent(typeof(Block))]
public class IceExplosion : MonoBehaviour {
	public int Range;
	public Transform IceParticles;

	private Block myBlock;


	private void Awake() {
		myBlock = GetComponent<Block>();
		myBlock.OnDestroy += () => { Explode(Range); };
	}


	public void Explode(int range) {
		if (myBlock == null)
			return;

		if (IceParticles != null) {
			Instantiate(IceParticles, transform.position, Quaternion.identity);
		}

		for (int y = -range; y <= range; y++) {
			for (int x = -range; x <= range; x++) {
				if (x == myBlock.BlockPosX && y == myBlock.BlockPosY)
					continue;

				float tempRange = (Mathf.Abs(x) + Mathf.Abs(y));
				float chance = tempRange / ((float)range * 2f);

				if (tempRange < Random.Range(0f, range * 2)) {
					Block b = GameManager.ins.GetBlockAtPos(myBlock.BlockPosX + x, myBlock.BlockPosY + y);

					if (b && !b.CustomFlags.Contains("ice")) {
						SpriteRenderer ice = Instantiate(GameManager.ins.IcePrefab, b.transform.position, Quaternion.identity);
						ice.sortingOrder = 6;
						ice.transform.parent = b.spriteRenderer.transform;
						ice.size = b.spriteRenderer.size;
						b.CustomFlags.Add("ice");
					}
				}
			}
		}
	}
}
