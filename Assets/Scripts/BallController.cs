﻿using UnityEngine;

public class BallController : MonoBehaviour {
	public float MinSpeed = 10;
	public float MaxSpeed = 50;

	private Rigidbody2D rigidbody2d;
	private float minSlope = 0.5f;


	private void Awake() {
		rigidbody2d = GetComponent<Rigidbody2D>();
		rigidbody2d.velocity = new Vector2(Random.Range(-MinSpeed, MinSpeed), Random.Range(-MinSpeed, MinSpeed));
	}


	private void FixedUpdate() {
		Vector2 vel = rigidbody2d.velocity;

		vel += vel.normalized * 0.1f * Time.fixedDeltaTime;

		if (vel.y < minSlope && vel.y >= 0) {
			vel.y = minSlope;
		} else if (vel.y > -minSlope && vel.y <= 0) {
			vel.y = -minSlope;
		}

		if (vel.magnitude < MinSpeed) {
			vel = vel.normalized * MinSpeed;
		} else if (vel.magnitude > MaxSpeed) {
			vel = vel.normalized * MaxSpeed;
		}

		rigidbody2d.velocity = vel;
	}


	private void OnCollisionEnter2D(Collision2D collision) {
		// Modify Health
		IHealth healthComp = collision.collider.GetComponent<IHealth>();

		if (healthComp != null) {
			healthComp.ModifyHealth(transform, -1);
		}

		// Bounce object
		Bounceable bounceable = collision.collider.GetComponent<Bounceable>();

		if (bounceable != null) {
			bounceable.DoBounce();
		}

		PadController padController = collision.collider.GetComponent<PadController>();

		if (padController != null) {
			float velocityStrength = rigidbody2d.velocity.magnitude;
			Vector3 dir = (transform.position - collision.collider.transform.position);
			dir.Normalize();
			dir.x /= 2f;
			rigidbody2d.velocity = dir * velocityStrength;
		}
	}
}
