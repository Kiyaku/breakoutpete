﻿using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
	public static GameManager ins;

	public MapData Map;
	public Block PeteBlock;
	public BallController BallPrefab;
	public SpriteRenderer IcePrefab;
	public List<Block> BlockPrefabs = new List<Block>();
	public List<Block> PowerupPrefabs = new List<Block>();
	public List<PadController> Pads = new List<PadController>();

	private Vector2 oldMousePos;
	private Camera mainCam;
	private List<Block> blocks = new List<Block>();


	private void Awake() {
		ins = this;

		float posX = -Map.SizeX;
		float posY = -Map.SizeY + Map.MapOffsetY;
		float blockSize = Block.BLOCK_WIDTH;
		int maxBlocksInRow = Mathf.FloorToInt(Map.SizeX / blockSize) * 2 + 1;
		int maxBlocksInCol = Mathf.FloorToInt(Map.SizeY / blockSize) * 2;

		Map.MapArray = new int[maxBlocksInRow, maxBlocksInCol];

		// Place Pete
		for (int i = 0; i < 3; i++) {
			int blockPosX = Random.Range(0, maxBlocksInRow);
			int blockPosY = Random.Range(1, maxBlocksInCol);

			if (!HasRoom(blockPosX, blockPosY, 2, 2))
				continue;

			posX = -Map.SizeX + blockPosX * blockSize;
			posY = 3 - blockPosY * blockSize;

			CreateBlock(blockPosX, blockPosY, 2, 2, PeteBlock);
		}

		int maxBigPieces = Random.Range(Map.SizeX * Map.SizeY * 2 / 2, Map.SizeX * Map.SizeY * 2 / 1);

		/*
		// Place Bigger Pieces
		for (int i = 0; i < maxBigPieces; i++) {
			int blockPosX = Random.Range(0, maxBlocksInRow);
			int blockPosY = Random.Range(1, maxBlocksInCol);
			int blockSizeX = Random.Range(2, 4);
			int blockSizeY = 1;

			if (!HasRoom(blockPosX, blockPosY, blockSizeX, blockSizeY))
				continue;

			posX = -Map.SizeX + blockPosX * blockSize;
			posY = 3 - blockPosY * blockSize;
			Block block = Instantiate(BlockPrefabs[blockPosY % BlockPrefabs.Count], GetBlockPos(blockPosX, blockPosY, blockSizeX, blockSizeY), Quaternion.identity);
			block.SetSize(blockSizeX, blockSizeY);

			PlaceBlock(blockPosX, blockPosY, blockSizeX, blockSizeY);
		}

		posX = -Map.SizeX;
		posY = 3;
		*/

		// Powerups
		for (int i = 0; i < 10; i++) {
			int blockPosX = Random.Range(0, maxBlocksInRow);
			int blockPosY = Random.Range(1, maxBlocksInCol);
			int blockSizeX = 2;
			int blockSizeY = 1;

			if (!HasRoom(blockPosX, blockPosY, blockSizeX, blockSizeY))
				continue;

			posX = -Map.SizeX + blockPosX * blockSize;
			posY = 3 - blockPosY * blockSize;

			CreateBlock(blockPosX, blockPosY, blockSizeX, blockSizeY, PowerupPrefabs[Random.Range(0, PowerupPrefabs.Count)]);
		}


		// Normal Fill Row
		for (int y = 0; y < maxBlocksInCol; y++) {
			for (int x = 0; x < maxBlocksInRow; x += 1) {
				int blockSizeX = Random.Range(1, 3);

				if (!HasRoom(x, y, blockSizeX, 1))
					continue;

				CreateBlock(x, y, blockSizeX, 1, BlockPrefabs[y % BlockPrefabs.Count]);
			}
		}


		// Fill board
		for (int y = 0; y < maxBlocksInCol; y++) {
			for (int x = 0; x < maxBlocksInRow; x++) {
				if (Map.MapArray[x, y] != 1) {
					CreateBlock(x, y, 1, 1, BlockPrefabs[y % BlockPrefabs.Count]);
				}
			}
		}
	}


	public void RemoveBlock(Block b) {
		blocks.Remove(b);
	}


	private void CreateBlock(int x, int y, int sizeX, int sizeY, Block block) {
		Block tempBlock = Instantiate(block, GetBlockPos(x, y, sizeX, sizeY), Quaternion.identity);
		tempBlock.SetSize(sizeX, sizeY);
		tempBlock.BlockPosX = x;
		tempBlock.BlockPosY = y;
		PlaceBlock(x, y, sizeX, sizeY);
		blocks.Add(tempBlock);
	}


	private Vector2 GetBlockPos(int x, int y, int sizeX, int sizeY) {
		return new Vector2(x * Block.BLOCK_WIDTH + Block.BLOCK_WIDTH / 2f * (sizeX - 1) - Map.SizeX, y * Block.BLOCK_WIDTH - Block.BLOCK_WIDTH / 2f * (sizeY - 1) + Map.MapOffsetY - Map.SizeY);
	}


	public Block GetBlockAtPos(int x, int y) {
		foreach (Block b in blocks) {
			if (b == null)
				continue;

			if (b.BlockPosX == x && b.BlockPosY == y) {
				return b;
			}
		}

		return null;
	}


	private bool HasRoom(int posX, int posY, int width, int height) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (posX + x >= Map.MapArray.GetLength(0))
					return false;

				if (posY - y < 0 || posY - y >= Map.MapArray.GetLength(1))
					return false;

				if (Map.MapArray[posX + x, posY - y] == 1) {
					return false;
				}
			}
		}

		return true;
	}


	private bool PlaceBlock(int posX, int posY, int width, int height) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (posX + x >= Map.MapArray.GetLength(0))
					return false;

				if (posY - y >= Map.MapArray.GetLength(1))
					return false;

				if (Map.MapArray[posX + x, posY - y] == 1) {
					return false;
				}

				Map.MapArray[posX + x, posY - y] = 1;
			}
		}

		return true;
	}


	private void Start() {
		mainCam = Camera.main;
	}


	public void Update() {
		Vector2 currentMousePos = mainCam.ScreenToWorldPoint(Input.mousePosition);
		Vector2 dir = currentMousePos - oldMousePos;
		dir.y = 0;

		foreach (var pad in Pads) {
			if(pad != null)
				pad.MovePad(dir);
		}

		oldMousePos = currentMousePos;

		if (Input.GetKeyDown(KeyCode.R)) {
			Application.LoadLevel(0);
		}
	}



	public void OnDrawGizmos() {
		Vector3 topLeft = new Vector3(-Map.SizeX, Map.SizeY + Map.MapOffsetY);
		Vector3 topRight = new Vector3(Map.SizeX, Map.SizeY + Map.MapOffsetY);
		Vector3 bottomLeft = new Vector3(-Map.SizeX, -Map.SizeY + Map.MapOffsetY);
		Vector3 bottomRight = new Vector3(Map.SizeX, -Map.SizeY + Map.MapOffsetY);
		Gizmos.color = Color.green;
		Gizmos.DrawLine(topLeft, topRight);
		Gizmos.DrawLine(topRight, bottomRight);
		Gizmos.DrawLine(bottomRight, bottomLeft);
		Gizmos.DrawLine(bottomLeft, topLeft);
	}
}
