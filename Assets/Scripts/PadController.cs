﻿using System.Collections;
using UnityEngine;

public class PadController : MonoBehaviour {
	public SpriteRenderer spriteRenderer;
	public SpriteRenderer InsideSpriteRenderer;
	public Transform SpriteMask;

	private BoxCollider2D boxCollider;


	public enum PadDirections {
		HORIZONTAL,
		VERTICAL
	}

	public PadDirections Direction;


	private void Awake() {
		boxCollider = GetComponent<BoxCollider2D>();
	}


	public void ResizePad(float amount) {
		Vector2 scale = spriteRenderer.size;
		scale.x *= amount;
		spriteRenderer.size = scale;
		boxCollider.size = spriteRenderer.size;

		Vector2 maskScale = SpriteMask.localScale;
		maskScale.x *= amount;
		SpriteMask.localScale = maskScale;
	}


	public void MovePad(Vector2 movedBy) {
		movedBy = transform.rotation * movedBy;
		Vector2 newPos = transform.position;
		newPos += movedBy;

		newPos.x = Mathf.Clamp(newPos.x, -GameManager.ins.Map.SizeX, GameManager.ins.Map.SizeX);

		transform.position = newPos;
		InsideSpriteRenderer.transform.localPosition = new Vector2(-newPos.x / 2f, 0f);
	}
}
