﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour, IHealth {
	public static readonly float BLOCK_WIDTH = 0.5f;

	public int StartHealth;
	[SerializeField]
	public int MaxHealth {
		get;
		set;
	}
	public int CurHealth {
		get;
		protected set;
	}
	public Transform BlockParticles;
	public List<Sprite> Sprites = new List<Sprite>();
	public SpriteRenderer spriteRenderer;
	public int BlockPosX, BlockPosY;

	[HideInInspector]
	public List<string> CustomFlags = new List<string>();

	protected BoxCollider2D collider2d;
	protected bool isAlive = true;
	protected int sizeX, sizeY;

	public System.Action OnDestroy;


	protected virtual void Awake() {
		CurHealth = MaxHealth = StartHealth;
		collider2d = GetComponent<BoxCollider2D>();
		collider2d.size = spriteRenderer.size;

		UpdateSprite();
	}


	protected virtual void OnDisable() {
		OnDestroy = null;
	}


	protected virtual void Start() {
		transform.parent = GameManager.ins.transform;
	}


	public int ModifyHealth(Transform source, int amount) {
		CurHealth += amount;
		ProcessDamage(source, amount);
		return CurHealth;
	}


	protected virtual void ProcessDamage(Transform source, int amount) {
		if (CurHealth <= 0 && isAlive) {
			isAlive = false;
			HandleDeath();
			return;
		}

		UpdateSprite();
	}


	public void SetSize(int x, int y) {
		sizeX = x;
		sizeY = y;
		spriteRenderer.size = new Vector2(x * BLOCK_WIDTH, y * BLOCK_WIDTH);
		collider2d.size = spriteRenderer.size;
	}


	protected virtual void HandleDeath() {
		CameraShake.ins.Shake(0.025f * sizeX, 0.1f);

		if (BlockParticles != null) {
			Instantiate(BlockParticles, transform.position, Quaternion.identity);
		}

		GameManager.ins.RemoveBlock(this);

		if (OnDestroy != null)
			OnDestroy();

		Destroy(gameObject);
	}


	protected void UpdateSprite() {
		int dmg = MaxHealth - CurHealth;
		if (Sprites.Count > dmg) {
			spriteRenderer.sprite = Sprites[dmg];
		}
	}
}
