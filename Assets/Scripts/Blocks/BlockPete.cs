﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockPete : Block {
	public Transform Pete;


	protected override void Start() {
		base.Start();

		CustomFlags.Add("ice");
		GetComponent<IceExplosion>().Explode(3);
	}


	protected override void HandleDeath() {
		//Pete.parent = null;
		base.HandleDeath();
	}
}
