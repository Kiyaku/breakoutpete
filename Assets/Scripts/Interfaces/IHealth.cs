﻿using UnityEngine;

public interface IHealth {
	int MaxHealth { get; set; }
	int CurHealth { get; }
	int ModifyHealth(Transform source, int amount);
}
