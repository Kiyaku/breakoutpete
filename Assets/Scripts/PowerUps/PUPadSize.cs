﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUPadSize : Powerup {
	public float ScaleFactor;
	public float ScaleDuration;


	protected override void ActivatePowerup(Transform activator) {
		StartCoroutine(ScalePad());
		collider2d.enabled = false;
		spriteRenderer.enabled = false;
	}


	private IEnumerator ScalePad() {
		foreach (var pad in GameManager.ins.Pads) {
			if (pad == null)
				continue;

			pad.ResizePad(ScaleFactor);
		}

		yield return new WaitForSeconds(ScaleDuration);

		foreach (var pad in GameManager.ins.Pads) {
			if (pad == null)
				continue;

			pad.ResizePad(1f / ScaleFactor);
		}

		base.ActivatePowerup(null);
	}
}
