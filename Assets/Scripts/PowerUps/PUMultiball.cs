﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PUMultiball : Powerup {
	public int ExtraBalls = 1;


	protected override void ActivatePowerup(Transform activator) {
		for (int i = 0; i < ExtraBalls; i++) {
			Instantiate(GameManager.ins.BallPrefab, activator.position, Quaternion.identity);
		}

		base.ActivatePowerup(activator);
	}
}
