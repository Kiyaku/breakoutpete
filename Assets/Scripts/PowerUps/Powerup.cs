﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : Block, IHealth {
	protected override void ProcessDamage(Transform source, int amount) {
		ActivatePowerup(source);
	}


	protected virtual void ActivatePowerup(Transform activator) {
		Destroy(gameObject);
	}
}
