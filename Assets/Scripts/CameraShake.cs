﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraShake : MonoBehaviour {
	public static CameraShake ins;

	private Vector3 startPos;


	private void Awake() {
		ins = this;
		startPos = transform.position;
	}


	public void Shake(float strength, float length) {
		StartCoroutine(shake(strength, length));
	}


	private IEnumerator shake(float strength, float length) {
		while (length >= 0) {
			length -= Time.deltaTime;
			transform.position = startPos + new Vector3(Random.Range(-strength, strength), Random.Range(-strength, strength), 0);

			yield return null;
		}

		transform.position = startPos;
	}
}
